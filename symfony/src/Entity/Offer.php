<?php

namespace App\Entity;

class Offer
{
    /** @var string */
    protected $type;

    /** @var string */
    protected $name;

    /** @var string */
    protected $description;

    /** @var  PromoCode[] */
    protected $promoCodes;

    /**
     * Offer constructor.
     */
    public function __construct()
    {
        $this->promoCodes = [];
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     * @return Offer
     */
    public function setType(string $type): Offer
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Offer
     */
    public function setName(string $name): Offer
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @param string $description
     * @return Offer
     */
    public function setDescription(string $description): Offer
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return PromoCode[]
     */
    public function getPromoCodes(): array
    {
        return $this->promoCodes;
    }

    /**
     * @param PromoCode[] $codes
     * @return Offer
     */
    public function setPromoCodes(array $codes): Offer
    {
        $this->promoCodes = $codes;
        return $this;
    }

    /**
     * @param PromoCode $code
     * @return Offer
     */
    public function addPromoCode(PromoCode $code): Offer
    {
        $storage = $this->__transformToStorage($this->getPromoCodes());

        if (!$storage->contains($code))
        {
            $this->promoCodes[] = $code;
        }

        return $this;
    }

    private function __transformToStorage(array $list): \SplObjectStorage
    {
        $storage = new \SplObjectStorage;

        foreach ($list as $item)
        {
            $storage->attach($item);
        }

        return $storage;
    }
}