<?php

namespace App\Command;

use App\Service\OfferManager;
use App\Service\PromoCodeManager;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Serializer\Exception\RuntimeException;
use Symfony\Component\Filesystem\Filesystem;
use App\Service\Writer\FileWriter;

class PromoCodeValidateCommand extends Command
{
    protected static $defaultName = 'promo-code:validate';

    /** @var string */
    protected $offerDir;

    /** @var OfferManager */
    protected $offerManager;

    /** @var PromoCodeManager */
    protected $promoCodeManager;

    /** @var Filesystem */
    protected $filesystem;

	/**
	 * PromoCodeValidateCommand constructor
	 * 
	 * @param OfferManager $offerManager
	 * @param PromoCodeManager $promoCodeManager
	 * @param Filesystem $filesystem
	 * @param string $offerDir
	 */
    public function __construct(OfferManager $offerManager, PromoCodeManager $promoCodeManager, Filesystem $filesystem, string $offerDir)
    {
        parent::__construct();

        $this->offerManager = $offerManager;
        $this->promoCodeManager = $promoCodeManager;
        $this->filesystem = $filesystem;
        $this->offerDir = $offerDir;
    }

    protected function configure()
    {
        $this
            ->setDescription("Check validity of the promo code.")
            ->setHelp("This Command allow you to check if the promo code is still valid, and save the offers.")
            ->addArgument('code',  InputArgument::REQUIRED, 'promo code')
            ->addOption('format', 'f',  InputOption::VALUE_OPTIONAL, 'format (json, xml)', "json")
            ->setHelp(<<<'EOF'
The <info>%command.name%</info> allow you to check if the promo code is still valid, 
and save the offers:

  <info>php %command.full_name% PROMO_CODE</info>
  <info>php %command.full_name% PROMO_CODE --format=xml</info>
EOF
            )
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);
        $code = $input->getArgument('code');
        $format = $input->getOption('format');

		$io->comment('Load promo code and offers from API...');
        $promoCodes = $this->promoCodeManager->loadFromApi();
        $offers = $this->offerManager->loadFromApi();

		$io->comment('Check if promo code is valid ...');
        if (!array_key_exists($code, $promoCodes)) {
            $io->error(sprintf("Promo code : %s does not exit", $code));
            return Command::FAILURE;
        }

        $selectedPromoCode = $promoCodes[$code];
        if ($selectedPromoCode->isExpired()) {
            $io->error(sprintf("Promo code: %s is no longer valid", $code));
            return Command::FAILURE;
        }

        $validOffers = $this->offerManager->findRelatedOffers($selectedPromoCode, $offers);
        if (empty($validOffers["compatibleOfferList"])) {
            $io->error(sprintf("No offer related to the promo code was found : %s", $code));
            return Command::FAILURE;
        }

		if(!$this->filesystem->exists($this->offerDir)){
			$this->filesystem->mkdir($this->offerDir, 0755);
		}
		
        if (!is_writable($this->offerDir)) {
            throw new RuntimeException(sprintf('Unable to write in the "%s" directory.', $this->offerDir));
        }
		
		$io->comment('Promo code is validate, we will generate the file ...');
        $formatClass = sprintf("App\Service\Formater\%sFormater", strtoupper($format));
        $writer = new FileWriter(new $formatClass(), $this->offerDir);
        $writer->write($validOffers);

		$io->success(sprintf("%s Valid Offer(s) exported in %s", count($validOffers["compatibleOfferList"]), $writer->getFilename()));
        return Command::SUCCESS;
    }

}
