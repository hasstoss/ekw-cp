<?php

namespace App\Service;

use App\Entity\Offer;
use App\Entity\PromoCode;

class OfferManager extends AbstractManager
{
    /**
     * OfferManager constructor.
     * @param string $url
     */
    public function __construct(string $url)
    {
        parent::__construct($url, Offer::class);
    }

    /**
     * @inheritDoc
     */
    public function deserialize(string $json): array
    {
        $offersDecode = json_decode($json, true);

        $offers = [];
        foreach ($offersDecode as $offerDecode) {
            $offer = new Offer();
            $offer
                ->setName($offerDecode['offerName'])
                ->setType($offerDecode['offerType'])
                ->setDescription($offerDecode['offerDescription'])
                ->setPromoCodes($offerDecode['validPromoCodeList'])
            ;
            $offers[] = $offer;
        }

        return $offers;
    }

    /**
     * find offer related to a promocode and build a response
     * @param PromoCode $code
     * @param array $offers
     * @return array
     */
    public function findRelatedOffers(PromoCode $code, array $offers) :array
    {
		$compatibleOfferList = [];
        $relatedOffers = [
            "promoCode" => $code->getCode(),
            "endDate" => $code->getEndDateFormated(),
            "discountValue" => $code->getDiscountValue(),
            "compatibleOfferList" => [],
        ];
        /** @var Offer $offer */
        foreach ($offers as $index => $offer) {
            if (in_array($code->getCode(), $offer->getPromoCodes())) {
                $compatibleOfferList[$index]["name"] = $offer->getName();
                $compatibleOfferList[$index]["type"] = $offer->getType();
            }
        }
		
		$relatedOffers["compatibleOfferList"] = array_values($compatibleOfferList);

        return $relatedOffers;
    }
}