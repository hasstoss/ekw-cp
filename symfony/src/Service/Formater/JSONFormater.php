<?php

namespace App\Service\Formater;

class JSONFormater implements FormaterInterface
{
    /**
     * @inheritDoc
     */
    public function format(array $data): string
    {
        return json_encode($data);
    }
	
    /**
     * @inheritDoc
     */
	public function getExtension(): string
	{
		return "json";
	}
}
