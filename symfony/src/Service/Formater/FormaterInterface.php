<?php

namespace App\Service\Formater;

interface FormaterInterface
{
    /**
     * formating data
     * @param array $data
     * @return string
     */
    public function format(array $data): string;
	
    /**
     * get extension for filename
     * @return string
     */
    public function getExtension(): string;
}
