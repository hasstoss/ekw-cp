<?php

namespace App\Service\Writer;

use App\Service\Formater\FormaterInterface;

abstract class AbstractWriter
{
    /** @var FormaterInterface */
    protected $formater;

    /**
     * Writer constructor.
	 * 
     * @param FormaterInterface $formater
     */
    public function __construct(FormaterInterface $formater)
    {
        $this->formater = $formater;
    }

    /**
     * write data in the selected format
     * @param array $data
     * @return mixed
     */
    abstract public function write(array $data);
}