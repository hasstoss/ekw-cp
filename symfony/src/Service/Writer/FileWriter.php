<?php

namespace App\Service\Writer;

use App\Service\Formater\FormaterInterface;

class FileWriter extends AbstractWriter
{
    /** @var string */
    protected $filename;

    /**
     * FileWriter constructor.
	 * 
     * @param FormaterInterface $formater
     * @param $filePath
     */
    public function __construct(FormaterInterface $formater, string $filePath)
    {
        parent::__construct($formater);
        $this->filename = $filePath . time() . '.' . $formater->getExtension();
    }

    /**
     * @inheritDoc
     */
    public function write (array $data)
    {
        $f = fopen($this->getFilename(), 'w');

        fwrite($f, $this->formater->format($data));

        fclose($f);
    }
	
	/**
	 * get filename
	 * 
	 * @return string
	 */
	public function getFilename(): string
	{
		return $this->filename;
	}
}