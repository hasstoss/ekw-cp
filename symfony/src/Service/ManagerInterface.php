<?php

namespace App\Service;

interface ManagerInterface
{
    /**
     * Get data from ekwateur API
     * @return array
     * @throws \Exception
     */
    public function loadFromApi(): array;

    /**
     * deserialize Object with data get from api
     * @param string $json
     * @return array
     */
    public function deserialize(string $json): array;
}