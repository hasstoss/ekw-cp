<?php

namespace App\Tests\Service;

use PHPUnit\Framework\TestCase;
use App\Service\OfferManager;
use App\Entity\PromoCode;

class OfferManagerTest extends TestCase
{
    /**
     * @dataProvider jsonDataProvider
     */
    public function testDeserialize($json)
    {
        $offerManager = new OfferManager("http://apiurl");

        $offersArray = json_decode($json, true);
        $offerDeserialize = $offerManager->deserialize($json);

        $this->assertEquals(count($offerDeserialize), count($offersArray));
        $this->assertEquals(current($offerDeserialize)->getName(), $offersArray[0]['offerName']);
    }
	
    /**
     * @dataProvider jsonOffersDataProvider
     */
    public function testFindRelatedOffersSuccess($json)
    {
        $offerManager = new OfferManager("http://apiurl");

        $offers = $offerManager->deserialize($json);
		
        $selectedPromoCode = new PromoCode();
        $selectedPromoCode
            ->setCode("ELEC_N_WOOD")
            ->setDiscountValue(1.5)
            ->setEndDate(new \DateTime("2022-06-20"))
		;
		
		$validOffers = $offerManager->findRelatedOffers($selectedPromoCode, $offers);

        $this->assertNotEmpty($validOffers["compatibleOfferList"]);
    }
	
    /**
     * @dataProvider jsonOffersDataProvider
     */
    public function testFindRelatedOffersError($json)
    {
        $offerManager = new OfferManager("http://apiurl");

        $offers = $offerManager->deserialize($json);
		
        $selectedPromoCode = new PromoCode();
        $selectedPromoCode
            ->setCode("NOT_FOUND")
            ->setDiscountValue(1.5)
            ->setEndDate(new \DateTime("2022-06-20"))
		;
		
		$validOffers = $offerManager->findRelatedOffers($selectedPromoCode, $offers);

        $this->assertEmpty($validOffers["compatibleOfferList"]);
    }

    public function jsonDataProvider()
    {
        return [
            ['[{"offerType":"GAS","offerName":"EKWAG2000","offerDescription":"Une offre incroyable","validPromoCodeList":["EKWA_WELCOME","ALL_2000"]}]'],
            ['[{"offerType":"GAS","offerName":"EKWAG3000","offerDescription":"Une offre croustillante","validPromoCodeList":["EKWA_WELCOME","GAZZZZZZZZY"]},{"offerType":"ELECTRICITY","offerName":"EKWAE2000","offerDescription":"Une offre du tonnerre","validPromoCodeList":["EKWA_WELCOME","ALL_2000","ELEC_IS_THE_NEW_GAS"]}]'],
            ['[{"offerType":"ELECTRICITY","offerName":"EKWAE3000","offerDescription":"Pile l offre qu il vous faut","validPromoCodeList":["EKWA_WELCOME","ELEC_IS_THE_NEW_GAS","BUZZ","ELEC_N_WOOD"]},{"offerType":"WOOD","offerName":"EKWAW2000","offerDescription":"Une offre du envoie du bois","validPromoCodeList":["EKWA_WELCOME","ALL_2000","WOODY","WOODY_WOODPECKER"]},{"offerType":"WOOD","offerName":"EKWAW3000","offerDescription":"Une offre souscrite = un arbre planté","validPromoCodeList":["EKWA_WELCOME","WOODY","WOODY_WOODPECKER","ELEC_N_WOOD"]}]']
        ];
    }

    public function jsonOffersDataProvider()
    {
        return [
            ['[{"offerType":"GAS","offerName":"EKWAG2000","offerDescription":"Une offre incroyable","validPromoCodeList":["EKWA_WELCOME","ALL_2000"]},{"offerType":"GAS","offerName":"EKWAG3000","offerDescription":"Une offre croustillante","validPromoCodeList":["EKWA_WELCOME","GAZZZZZZZZY"]},{"offerType":"ELECTRICITY","offerName":"EKWAE2000","offerDescription":"Une offre du tonnerre","validPromoCodeList":["EKWA_WELCOME","ALL_2000","ELEC_IS_THE_NEW_GAS"]},{"offerType":"ELECTRICITY","offerName":"EKWAE3000","offerDescription":"Pile l offre qu il vous faut","validPromoCodeList":["EKWA_WELCOME","ELEC_IS_THE_NEW_GAS","BUZZ","ELEC_N_WOOD"]},{"offerType":"WOOD","offerName":"EKWAW2000","offerDescription":"Une offre du envoie du bois","validPromoCodeList":["EKWA_WELCOME","ALL_2000","WOODY","WOODY_WOODPECKER"]},{"offerType":"WOOD","offerName":"EKWAW3000","offerDescription":"Une offre souscrite = un arbre planté","validPromoCodeList":["EKWA_WELCOME","WOODY","WOODY_WOODPECKER","ELEC_N_WOOD"]}]']
        ];
    }
}