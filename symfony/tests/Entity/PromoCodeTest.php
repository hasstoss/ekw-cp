<?php

namespace App\Tests\Entity;

use App\Entity\PromoCode;
use PHPUnit\Framework\TestCase;

class PromoCodeTest extends TestCase
{
    public function testIsNotExpired()
    {
        $promo = new PromoCode();
        $promo
            ->setCode("CODE-PROMO-1")
            ->setDiscountValue(10)
            ->setEndDate(new \DateTime("2022-12-23"))
		;

        $this->assertEquals(false, $promo->isExpired());
    }

    public function testIsExpired()
    {
        $promo = new PromoCode();
        $promo
            ->setCode("CODE-PROMO-2")
            ->setDiscountValue(20)
            ->setEndDate(new \DateTime("2012-12-23"))
        ;

        $this->assertEquals(true, $promo->isExpired());
    }
}