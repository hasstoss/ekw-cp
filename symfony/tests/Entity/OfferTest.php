<?php

namespace App\Tests\Entity;

use App\Entity\Offer;
use App\Entity\PromoCode;
use PHPUnit\Framework\TestCase;

class OfferTest extends TestCase
{
    public function testAddAndSetPromoCodes()
    {
        $offerAdd = new Offer();
        $offerAdd
            ->setName("Offer 1")
            ->setType("Type 1")
            ->setDescription("Offer Description")
            ;

        $offerSet = new Offer();
        $offerSet
            ->setName("Offer 1")
            ->setType("Type 1")
            ->setDescription("Offer Description")
        ;

        $codes = [];
        for ($i = 0; $i < 3; $i++)
        {
            $newCode = "promo".$i;
            $newCode = new PromoCode();
            $newCode
                ->setCode("TEST_PROMO".$i)
                ->setDiscountValue(10)
                ->setEndDate(new \DateTime("2022-12-23"))
            ;
            $offerAdd->addPromoCode($newCode);
            $codes[] = $newCode;
        }

        $offerSet->setPromoCodes($codes);


        $this->assertEquals(count($offerSet->getPromoCodes()), count($offerAdd->getPromoCodes()));
        $this->assertEquals($offerSet, $offerAdd);
    }
}