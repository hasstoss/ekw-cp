# Test technique Symfony - ekWateur

A [Docker](https://www.docker.com/)-based installer and runtime for the [Symfony](https://symfony.com) skeleton framework.

## How to install?

1. If not already done, [install Docker Compose](https://docs.docker.com/compose/install/)

#### Clone repository:

```
#git clone git@gitlab.com:hasstoss/ekw-cp.git YOUR_PROJECT
#composer install
```

#### Run:

```
#docker-compose up -d
```


#### Stop:

```
#docker-compose stop
```

#### Run php scripts from the console:

```
#docker-compose exec php sh
```

if you are under Windows run:

```
#winpty docker-compose exec php sh
```

## Getting Started

#### Run Unit Tests:

```
#bin/phpunit
```

#### Run the Promo Code Validate Console:

```
#bin/console promo-code:validate PUT_YOUR_CODE
```
